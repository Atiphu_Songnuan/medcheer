<?php
class ApiService
{
    public function __construct()
    {

    }

    public function loadModel($name)
    {
        $path = 'models/' . $name . '_model.php';
        if (file_exists($path)) {
            require_once 'models/' . $name . '_model.php';
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }

    //******** Login ********//
    public function Login()
    {
        $this->loadModel("login");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->Login($request->username, $request->password);
    }
    //*******************************************************//

    //******** Main ********//
    public function GetPersonDataByID()
    {
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->GetPersonDataByID($request->perid);
    }

    public function InsertPersonCheer()
    {
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->InsertPersonCheer($request);
    }

    function CheckDuplicatePersonByID(){
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $this->model->CheckDuplicatePersonByID($request->perid);
    }
    //*******************************************************//

    public function SessionDestroy()
    {
        // session_unset();
        session_destroy();
    }
}
