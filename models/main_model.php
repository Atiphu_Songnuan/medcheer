<?php
class Main_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function CheckDuplicatePersonByID($perid){
        $sql = 'SELECT PERID, NAME, SURNAME, CHECK_IN_DATE FROM medpersoncheer WHERE PERID = ' . $perid . '';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }

    // For Modal
    public function GetPersonDataByID($perid)
    {
        // echo json_encode($perid);
        // $encodePassword = md5($password);
        $sql = 'SELECT PERID, NAME, SURNAME, Dep_name FROM viewpersonindepart WHERE PERID = ' . $perid . '';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }
    // *************************************

    function InsertPersonCheer($persondata){
        // echo $persondata->name;
        if (isset($persondata)) {
            $data = [
                'perid' => $persondata->perid,
                'name' => $persondata->name,
                'surname' => $persondata->surname,
                'department' => $persondata->department,
                'color' => $persondata->color
            ];

            $sql = 'INSERT INTO  medpersoncheer(PERID, NAME, SURNAME, DEPARTMENT, COLOR, CHECK_IN_DATE)
                    VALUES  (:perid, :name, :surname, :department, :color, NOW())';

            $sth = $this->db->prepare($sql);
            $sth->execute($data);

            echo true;
        } else{
            echo false;
        }
    }

    // For Datatable
    public function GetPersonCheerData()
    {
        // echo json_encode($perid);
        // $encodePassword = md5($password);
        $sql = 'SELECT PERID, NAME, SURNAME, DEPARTMENT, COLOR, CHECK_IN_DATE FROM medpersoncheer WHERE DATE(CHECK_IN_DATE) = CURDATE() ORDER BY CHECK_IN_DATE DESC';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        return $jsonData;
    }
}
