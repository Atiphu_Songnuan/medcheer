<?php
class Login extends Controller
{
    public function __construct()
    {
        parent::__construct('login');
    }

    public function index()
    {
        $this->views->render('login/index');
    }

}
