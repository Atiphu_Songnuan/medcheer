<?php
class Main extends Controller
{
    public function __construct()
    {
        parent::__construct('main');
        $this->views->PersonCheerData = $this->model->GetPersonCheerData();
    }
    

    public function index()
    {
        $this->views->render('main/index');
    }

}
