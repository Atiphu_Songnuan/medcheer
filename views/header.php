<header class="main-header">

    <?php 
        // echo $_SESSION['USERNAME'];
        if (!isset($_SESSION['USERNAME']) && !isset($_SESSION['PASSWORD'])) {
            echo ("<script>
                  window.location.href='../medcheer/login';
                  </script>");
        }
    ?>

    <!-- <nav class="navbar navbar-expand  topbar mb-4 static-top shadow" style="background: #2193b0;  /* fallback for old browsers */
background: -webkit-linear-gradient(to bottom, #6dd5ed, #2193b0);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to bottom, #6dd5ed, #2193b0); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
"> -->
<nav class="navbar navbar-expand  topbar mb-4 static-top shadow" style="background-color: #8ba892">

        <!-- Sidebar Toggle (Topbar) -->
        <!-- <button id="sidebarToggleTop" class="btn btn-link d-md-none round-circle mr-3">
            <i class="fa fa-bars"></i>
        </button> -->
        <h3>MED|<small>Cheer</small></h3>
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">   
            
            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="img-profile rounded-circle" src="./public/images/man.svg">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal" onclick="Logout()">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-500"></i>
                Logout
                </a>
            </div>
            </li>
        </ul>
    </nav>
</header>

<script>
    function Logout(){
        Swal.fire({
        title: 'ต้องการออกจากระบบหรือไม่?',
        // text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ใช่',
        cancelButtonText: 'ยกเลิก'
        }).then((result) => {
        if (result.value) {
            $.ajax({
            type:"POST",
            url:"../medcheer/ApiService/SessionDestroy",
            success: function() {
                Swal.fire({
                title: 'ออกจากระบบสำเร็จ',
                text: '',
                type: 'success',
                timer: 2000,
                showConfirmButton: false,
                onClose: () =>{
                    window.location.href='../medcheer/login';
                }
                });
            },
            error: function() {
                console.log('Error occured');
            }
            });
        }
        });
    }
</script>