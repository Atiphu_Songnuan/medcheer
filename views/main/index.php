<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>MedCheer</title>
    
    <link rel="icon" type="image/png" href="./public/images/icons/megaphone.ico"/>

    <link href="./public/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="./public/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    

    <!-- SB Admin 2 -->
    <link href="./public/css/sb-admin-2.min.css" rel="stylesheet">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="./public/vendor/sweetalert2/dist/sweetalert2.min.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">
    <style>
        body {
            font-family: 'Kanit', serif;
            font-size: 28px;
        }
        label{
            font-size: 18px;
        }
        h4{
            font-size: 24px;
        }
        h3{
            color: white;
            font-size: 28px;
        }
        option{
            font-size: 14px;
        }
        .table-responsive {
            min-height: .01%;
            overflow-x: hidden;
        }
        div.dataTables_info , a{
            font-size: 14px;
        }
        div.table-responsive > div.dataTables_wrapper > div.row > div[class^="col-"]:last-child {
            padding-right: 0;
            overflow-x: auto;
        }

        table.table-bordered.dataTable tbody th, table.table-bordered.dataTable tbody td {
            border-bottom-width: 0;
            /* width: 100px; */
            white-space: nowrap;
            vertical-align: middle;
            width: 1%;
        }

        .table>caption+thead>tr:first-child>td, .table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>td, .table>thead:first-child>tr:first-child>th {
            border-top: 0;
            white-space: nowrap;
            width: 1%;
        }

        td{
            white-space:nowrap;
            font-size: 14px;
            /* vertical-align: middle; */
        }
        tr{
            font-family: 'Kanit', serif;
            font-size: 16px;
        }

        .edit-modal .modal {
            position: relative;
            top: auto;
            bottom: auto;
            right: auto;
            left: auto;
            display: block;
            z-index: 1;
        }
        .edit-modal .modal {
            background: transparent !important;
        }
        .swal2-popup {
		font-size: 0.7rem !important;
	    }
    </style>
</head>
<body class="wrapper">

    <!-- Require Header from header.php -->
    <?php require './views/header.php'?>

    <div class="container-fluid">
        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4" >
            <h4 class="mb-0 text-gray-800">ค้นหาบุคลากร</h4>
        </div>

        <div class="row">
            <!-- Card 1 -->
            <div class="col-xl-12 col-md-12 mb-12">
                <div class="form-group">
                    <input class="form-control" type="number" placeholder="รหัสบุคลากร" id="inputperid">
                    <button class="btn btn-primary form-control" onclick="SearchPersonal()" id="btnsearch">ค้นหา</button>
                    <!-- <button class="btn btn-primary form-control" onclick="SearchPersonal()">ค้นหา</button> -->
                </div>
            </div>

            <div class="col-xl-12 col-md-12 mb-12">
                <div class="table-responsive">
                    <table class="table table-bordered" id="personcheerdatatable" width="100%" cellspacing="0">
                        <thead>
                            <tr class="text-gray-800">
                                <th>วันที่</th>
                                <th>รหัสพนักงาน</th>
                                <th>หน่วยงาน</th>
                                <th>ชื่อ-นามสกุล</th>
                                <th>สี</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $personcheerJsonDecode = json_decode($this->PersonCheerData,true);
                                for ($i=0; $i < count($personcheerJsonDecode); $i++) { 
                                    // $personid = $personcheerJsonDecode[$i]['PERID'];
                                    echo '<tr class="text-gray-800">';
                                        echo '<td align="center">';
                                            if ($personcheerJsonDecode[$i]['CHECK_IN_DATE'] != "") {
                                                echo $personcheerJsonDecode[$i]['CHECK_IN_DATE'];
                                            } else{
                                                echo "-";
                                            }
                                        echo '</td>';

                                        echo '<td align="center">';
                                            if ($personcheerJsonDecode[$i]['PERID'] != "") {
                                                echo $personcheerJsonDecode[$i]['PERID'];
                                            } else{
                                                echo "-";
                                            }
                                        echo '</td>';

                                        echo '<td>';
                                            if ($personcheerJsonDecode[$i]['DEPARTMENT'] != "") {
                                                echo $personcheerJsonDecode[$i]['DEPARTMENT'];
                                            } else{
                                                echo "-";
                                            }
                                        echo '</td>';

                                        echo '<td>';
                                            if ($personcheerJsonDecode[$i]['NAME'] != "" && $personcheerJsonDecode[$i]['SURNAME'] != "") {
                                                echo $personcheerJsonDecode[$i]['NAME']. " " . $personcheerJsonDecode[$i]['SURNAME'];
                                            } else{
                                                echo "-";
                                            }
                                        echo '</td>';

                                        $color = "";
                                        switch ($personcheerJsonDecode[$i]['COLOR']) {
                                            case 'ฟ้า':
                                                # code...
                                                $color = "#95DEE3";
                                                break;
                                            case 'เหลือง':
                                                # code...
                                                $color = "#F6D155";
                                                break;
                                            case 'ชมพู':
                                                # code...
                                                $color = "#E8B5CE";
                                                break;
                                            case 'ม่วง':
                                                # code...
                                                $color = "#6B5B95";
                                                break;
                                            default:
                                                # code...
                                                break;
                                        }

                                        echo '<td align="center" style="background-color:'.$color.'; color:white">';
                                            if ($personcheerJsonDecode[$i]['COLOR'] != "") {
                                                echo $personcheerJsonDecode[$i]['COLOR'];
                                            } else{
                                                echo "-";
                                            }
                                        echo '</td>';
                                    echo '</tr>';
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- End Card 1 -->
        </div>
    </div>

    <div class="modal fade" id="personal-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="col-12 modal-title">ข้อมูลบุคลากร</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row text-center">
                        <div class="col-md-12">
                        <!-- 41701 -->
                            <img id="personimg" alt="leaf" width="150px" height="150px">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <label><b>รหัสบุคลากร: </b></label>
                            <label id="peridlabel"></label>
                        </div>

                        <div class="col-md-6">
                            <label><b>หน่วยงาน: </b></label>
                            <label id="departlabel"></label>
                        </div>

                        <div class="col-md-6">
                            <label><b>ชื่อ-นามสกุล: </b></label>
                            <label id=namelabel></label>&nbsp<label id="surnamelabel"></label>
                        </div>

                        <div class="col-md-12">
                            <label><b>สี</b></label>
                            <select class="form-control" name="selectcolor" id="color">
                                <option value="0">เลือกสี</option>
                                <option value="1">ฟ้า</option>
                                <option value="2">เหลือง</option>
                                <option value="3">ชมพู</option>
                                <option value="4">ม่วง</option>
                            </select>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ยกเลิก</button>
                    <button type="button" onclick="PersonCheerConfirm()" id="btnconfirm" class="btn btn-primary">บันทึก</button>
                </div>
            </div>
        </div>
    </div>

   <!-- Bootstrap core JavaScript-->
   <script src="./public/vendor/jquery/jquery.min.js"></script>
    <script src="./public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Datatable -->
    <script src="./public/vendor/datatables/jquery.dataTables.js"></script>
    <script src="./public/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Demo scripts for this page-->
    <script src="./public/js/demo/datatables-demo.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="./public/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="./public/js/sb-admin-2.min.js"></script>

    <!-- SweetAlert2 -->
    <script src="./public/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

    <script>
        $(function(){
            $('#personcheerdatatable').dataTable({
                "order": [[ 0, "desc" ]],
                "columnDefs": [
                    {
                        "targets": [ 0 ],
                        "visible": false, //ซ่อน column ตาม targets ที่กำหนดไว้
                        "searchable": false
                    }
                ]
            });
        });

        // Press Enter to Login
        $('body').keyup(function(e) {
            // console.log('keyup called');
            var code = e.keyCode || e.which;
            if (code == '13') {
                SearchPersonal();
                $('#btnsearch').focus();
            }
        });

        function SearchPersonal(){
            var jsondata = {"perid": $('#inputperid').val()};
            // console.log(jsondata);

            $.ajax({
                type:"POST",
                url:"../medcheer/ApiService/CheckDuplicatePersonByID",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: JSON.stringify(jsondata),
                contentType: "application/json",
                dataType: "json",
                success: function(response) {
                    if (response.length != 0) {
                        var curDate = new Date();
                        var year = curDate.getFullYear();
                        var month = curDate.getMonth()+1;
                        if (month < 10) {
                            month = "0" + month;
                        }
                        var day = curDate.getDate();
                        var today = year + '-' + month + '-' + day;

                        var splitDateTime = response[0]['CHECK_IN_DATE'].split(' ');
                        var date =splitDateTime[0];

                        if (date === today) {
                            Swal.fire({
                                title: 'วันนี้ได้ลงชื่อแล้ว ไม่สามารถลงชื่อได้อีก',
                                text: '',
                                type: 'warning',
                                timer: 3000,
                                showConfirmButton: false,
                            })
                        } else{
                            $.ajax({
                                type:"POST",
                                url:"../medcheer/ApiService/GetPersonDataByID",
                                headers: {
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                },
                                data: JSON.stringify(jsondata),
                                contentType: "application/json",
                                dataType: "json",
                                success: function(response) {
                                    // console.log(response);
                                    if (response.length != 0) {
                                        $('#personal-modal').modal('show');
                                        for (let index = 0; index < response.length; index++) {
                                            const dataElement = response[index];
                                            $('#personimg').attr("src", "<?php echo PERSONALPICTURE;?>" + dataElement['PERID'] );
                                            $('#peridlabel').text(dataElement['PERID']);      
                                            $('#departlabel').text(dataElement['Dep_name']);
                                            $('#namelabel').text(dataElement['NAME']);      
                                            $('#surnamelabel').text(dataElement['SURNAME']);      
                                            // console.log(dataElement['PERID']);                      
                                        }
                                    } else{
                                        Swal.fire({
                                            title: 'ไม่พบข้อมูลบุคลากร',
                                            text: '',
                                            type: 'error',
                                            timer: 2000,
                                            showConfirmButton: false,
                                        })
                                    }
                                },
                                error: function(response){
                                    console.log(response.responseText);
                                }
                            });
                        }
                    }  else{
                        $.ajax({
                            type:"POST",
                            url:"../medcheer/ApiService/GetPersonDataByID",
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            data: JSON.stringify(jsondata),
                            contentType: "application/json",
                            dataType: "json",
                            success: function(response) {
                                // console.log(response);
                                if (response.length != 0) {
                                    $('#personal-modal').modal('show');
                                    for (let index = 0; index < response.length; index++) {
                                        const dataElement = response[index];
                                        $('#personimg').attr("src", "<?php echo PERSONALPICTURE;?>" + dataElement['PERID'] );
                                        $('#peridlabel').text(dataElement['PERID']);      
                                        $('#departlabel').text(dataElement['Dep_name']);
                                        $('#namelabel').text(dataElement['NAME']);      
                                        $('#surnamelabel').text(dataElement['SURNAME']);      
                                        // console.log(dataElement['PERID']);                      
                                    }
                                } else{
                                    Swal.fire({
                                        title: 'ไม่พบข้อมูลบุคลากร',
                                        text: '',
                                        type: 'error',
                                        timer: 2000,
                                        showConfirmButton: false,
                                    })
                                }
                            },
                            error: function(response){
                                console.log(response.responseText);
                            }
                        });
                    }
                },
                error: function(response){
                    console.log(response.responseText);
                }
            });      
        }

        function PersonCheerConfirm(){
            if ($('#color option:selected').val() == 0) {
                Swal.fire({
                    title: 'กรุณาเลือกสี',
                    text: '',
                    type: 'warning',
                    timer: 2000,
                    showConfirmButton: false
                })
            }else{
                var jsondata = {
                    "perid":$('#peridlabel').text(),
                    "name":$('#namelabel').text(),
                    "surname":$('#surnamelabel').text(),
                    "department":$('#departlabel').text(),
                    "color": $('#color option:selected').text(),
                }
                $.ajax({
                    type:"POST",
                    url:"../medcheer/ApiService/InsertPersonCheer",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    data: JSON.stringify(jsondata),
                    contentType: "application/json",
                    dataType: "json",
                    success: function() {
                        Swal.fire({
                            title: 'เพิ่มข้อมูลสำเร็จ',
                            text: '',
                            type: 'success',
                            timer: 2000,
                            showConfirmButton: false,
                            onClose: () =>{
                                window.location.href='../medcheer/main';
                            }
                        })
                    },
                    error: function(response) {
                        console.log(response.responseText);
                    }
                });
            }
        }
    </script>
</body>
</html>